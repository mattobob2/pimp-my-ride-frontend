# PimpMyRideFrontend

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 11.2.4.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI Overview and Command Reference](https://angular.io/cli) page.

## Aufgabenstellung

Sie wurden damit beauftragt einen KFZ-Konfigurator (Webapplikation) zu entwickeln, 
welcher die Konfiguration eines Autos mit den folgenden Optionen ermoeglicht:

1. Motorleistung
1. Lackierung
1. Felgen
1. Sonderausstattungen (max. 5 Stueck, z.B. Klimaanlage, Soundsystem, Fahrsicherheitssysteme etc.)

## Beschreibung

Jede Veraenderung an der Konfiguration soll sich unmittelbar und ohne einen Page-Refresh auf den angezeigten Preis auswirken.
Am Ende der Konfiguration soll eine Zusammenfassung angezeigt und die Bestellung abgesendet werden koennen.
Zudem soll eine Url generiert werden, mit der der Benutzer jederzeit Zugriff auf die gewaehlte Konfiguration hat.
Sowohl die Konfigurationseigenschaften als auch die Bestellungen sind in einer Datenbank zu speichern.
Als Hilfestellung zum Design und zur Benutzerführung koennen die zahlreichen am Markt 
verfügbaren Konfiguratoren als Orientierung dienen. Die Implementierung einer Authentifizierungs- bzw.
Authorisierungslogik fuer Anwender ist nicht erforderlich.

## Technischer Rahmen
 
1. Frontend auf Basis von wahlweise Vue.js, React oder Angular
1. Backend (Services) auf Basis von Java
1. Betrieb wahlweise Serverless, Container oder Webserver, gerne auch in der Cloud
1. Gerne wuerde ich eine funktionierende CI/CD-Pipeline sehen, die einen Code-Change auf das Zielsystem deployed
