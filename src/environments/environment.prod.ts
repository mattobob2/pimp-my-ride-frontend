export const environment = {
  production: true,
  backendServiceUrl: 'https://pymp-my-ride.azurewebsites.net',
  frontendUrl: 'https://pymp-my-ride-frontend.azurewebsites.net'
};
