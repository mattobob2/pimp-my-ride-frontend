import {Injectable} from '@angular/core';
import {Article} from '../model/article.model';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Configuration} from '../model/configuration.model';
import {ARTICLE_TYPES} from '../model/article-types.enum';
import {OrderResponse} from '../model/order-response.model';
import {environment} from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class BackendService {


  constructor(private http: HttpClient) {
  }

  getArticles(type: ARTICLE_TYPES): Observable<Array<Article>> {
    const url = environment.backendServiceUrl + '/rest/get' + type;
    return this.http.get<Array<Article>>(url);
  }

  getConfiguration(configurationId: string): Observable<Configuration> {
    const url = environment.backendServiceUrl + '/rest/getOrder/' + configurationId;
    return this.http.get<Configuration>(url);
  }

  saveOrder(configuration: Configuration): Observable<OrderResponse> {
    const url = environment.backendServiceUrl + '/rest/saveOrder';
    return this.http.post<OrderResponse>(url, configuration);
  }
}
