import {Component} from '@angular/core';
import {BackendService} from './service/backend.service';
import {ARTICLE_TYPES} from './model/article-types.enum';
import {ActivatedRoute} from '@angular/router';
import {Article} from './model/article.model';
import {FormControl, Validators} from '@angular/forms';
import {Configuration} from './model/configuration.model';
import {OrderResponse} from './model/order-response.model';
import {environment} from '../environments/environment';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  constructor(private route: ActivatedRoute, private backendService: BackendService) {

  }

  title = 'pimp-my-ride';

  // Motor
  motors: Array<Article>;
  selectedMotor: Article;

  // Rim
  rims: Array<Article>;
  selectedRim: Article;

  // Paint
  paints: Array<Article>;
  selectedPaint: Article;

  // Optionals
  optionals: Array<Article>;
  selectedOptionals: Array<Article> = new Array<Article>();

  // Configuration
  configuration: Configuration;

  // Total Price
  totalPrice = 0;

  // OrderUrl
  orderUrl = '';
  orderResponse: OrderResponse;
  orderId = '';

  motorControl = new FormControl('', Validators.required);
  rimControl = new FormControl('', Validators.required);
  paintControl = new FormControl('', Validators.required);
  optionalControl = new FormControl('', Validators.required);

  ngOnInit() {
    this.route.queryParams.subscribe(params => {
      this.initializePage(params.orderId);
    });
  }

  initializePage(orderId: string): void {
    console.log('Getting all articles...');
    this.backendService.getArticles(ARTICLE_TYPES.MOTORS).subscribe(retrievedMotors => {
      this.motors = retrievedMotors;
      this.backendService.getArticles(ARTICLE_TYPES.RIMS).subscribe(retrievedRims => {
        this.rims = retrievedRims;
        this.backendService.getArticles(ARTICLE_TYPES.PAINTS).subscribe(retrievedPaints => {
          this.paints = retrievedPaints;
          this.backendService.getArticles(ARTICLE_TYPES.OPTIONALS).subscribe(retrievedOptionals => {
            this.optionals = retrievedOptionals;
            if (orderId !== undefined && orderId !== '') {
              console.log('Setting order configuration: ', orderId);
              this.retrieveAndSetOrderConfiguration(orderId);
            } else {
              console.log('Setting default configuration');
              this.setDefaultConfiguration();
            }
          });
        });
      });
    });
  }

  setSelectedMotor(motor: Article): void {
    this.selectedMotor = motor;
    console.log('articleNr: ' + this.selectedMotor.articleNr + '; the price for this motor is:', this.selectedMotor.price);
    this.calculateTotalPrice();
  }

  setSelectedRim(rim: Article): void {
    this.selectedRim = rim;
    console.log('articleNr: ' + this.selectedRim.articleNr + '; the price for this Rim is:', this.selectedRim.price);
    this.calculateTotalPrice();
  }

  setSelectedPaint(paint: Article): void {
    this.selectedPaint = paint;
    console.log('articleNr: ' + this.selectedPaint.articleNr + '; the price for this Paint is:', this.selectedPaint.price);
    this.calculateTotalPrice();
  }

  setSelectedOptionals(extraOptionals: Array<Article>): void {
    console.log('extraOptionals: ', extraOptionals);
    this.selectedOptionals = extraOptionals;
    this.calculateTotalPrice();
  }

  calculateTotalPrice(): void {
    let optionalsPrice = 0;
    this.selectedOptionals.forEach(value => optionalsPrice += value.price);
    const motorPrice = this.selectedMotor !== undefined ? this.selectedMotor.price : 0;
    const rimPrice = this.selectedRim !== undefined ? this.selectedRim.price : 0;
    const paintPrice = this.selectedPaint !== undefined ? this.selectedPaint.price : 0;
    this.totalPrice = motorPrice + rimPrice + paintPrice + optionalsPrice;
    console.log('Total Price for this order: ', this.totalPrice);

    this.setOrderConfiguration();
  }

  setOrderConfiguration(): void {
    this.configuration = new Configuration();
    this.configuration.motor = this.selectedMotor;
    this.configuration.rim = this.selectedRim;
    this.configuration.paint = this.selectedPaint;
    this.configuration.extraOptionals = this.selectedOptionals;
  }

  sendOrder(): void {
    console.log('Saving Configuration: ', this.configuration);
    this.backendService
      .saveOrder(this.configuration)
      .subscribe(value => {
        this.orderResponse = value;
        this.orderUrl = environment.frontendUrl + '?orderId=' + value.configurationId;
      });
  }

  retrieveAndSetOrderConfiguration(configurationId: string): void {
    this.orderId = configurationId;
    this.backendService.getConfiguration(configurationId)
      .subscribe(value => {
        this.setDefaultConfiguration();

        this.motors.forEach(motorIt => {
          if (motorIt.articleNr === value.motor.articleNr) {
            this.selectedMotor = motorIt;
            this.motorControl.setValue(motorIt);
          }
        });
        this.rims.forEach(rimIt => {
          if (rimIt.articleNr === value.rim.articleNr) {
            this.selectedRim = rimIt;
            this.rimControl.setValue(rimIt);
          }
        });
        this.paints.forEach(paintIt => {
          if (paintIt.articleNr === value.paint.articleNr) {
            this.selectedPaint = paintIt;
            this.paintControl.setValue(paintIt);
          }
        });
        this.selectedOptionals = new Array<Article>();
        this.optionals.forEach(optionalsIt1 => {
          value.extraOptionals.forEach(optionalsIt2 => {
            if (optionalsIt1.articleNr === optionalsIt2.articleNr) {
              this.selectedOptionals.push(optionalsIt1);
            }
          });
        });
        this.optionalControl.setValue(this.selectedOptionals);
        this.orderUrl = environment.frontendUrl + '?orderId=' + value.configurationId;
        this.calculateTotalPrice();
      });
  }

  setDefaultConfiguration(): void {
    console.log('configuration motor: ', this.motors[0]);
    this.selectedMotor = this.motors[0];
    this.motorControl.setValue(this.motors[0]);
    this.selectedRim = this.rims[0];
    this.rimControl.setValue(this.selectedRim);
    this.selectedPaint = this.paints[0];
    this.paintControl.setValue(this.selectedPaint);
    this.selectedOptionals.push(this.optionals[0]);
    this.optionalControl.setValue(this.selectedOptionals);
    this.calculateTotalPrice();
  }

}
