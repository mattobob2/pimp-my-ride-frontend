export class Article {
  id: number;
  articleNr: string;
  description: string;
  price: number;
}
