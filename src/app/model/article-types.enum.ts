export enum ARTICLE_TYPES {
  MOTORS = 'Motors',
  RIMS = 'Rims',
  PAINTS = 'Paints',
  OPTIONALS = 'Optionals'
}
