import {Article} from '../model/article.model';

export class Configuration {
  public configurationId: string;
  public motor: Article;
  public rim: Article;
  public paint: Article;
  public extraOptionals: Array<Article>;
}
